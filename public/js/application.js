var app= angular.module("pradeepApp",["ui.router","angularSpectrumColorpicker"])

app.config(["$stateProvider","$urlRouterProvider",function($stateProvider,$urlRouterProvider){
   $urlRouterProvider.otherwise("/");	
   $stateProvider
   .state("iot",{
   	  url:"/",
   	  templateUrl:"/js/template/app.html",
   	  controller:function($scope,$state){
   	  	$scope.list= [{name:"Bulb",id:"bulp",icon:"light"},{name:"Ketle",id:"ketle"},{name:"Fan",id:"fan"},{name:"Fridge",id:"fridge"},{name:"Door",id:"door"}];
   	  }
   })
   .state("bulb",{
   	  url:"/bulb",
   	  templateUrl:"/js/template/iot-item.html",
   	  controller:function($scope,$http){
          $scope.colorPicker ={
             color:"rgb(61, 188, 172)",
             rgb: null,
             intensity:null,
             isOn:false
           };
           $scope.onAndOff =function(data){
              //console.log(data);
           	  data.isOn = !data.isOn;
              data.color =null;
           	  $http.post("/handler",data)
           	    .success(function(data){
           	    	console.log(data);
           	    });
           } 
           $scope.changeColor =function(color){
             ///console.log(color);
             var rgb = color.toString().replace(/[\s\)rgba\(]+/g,"").split(",");
             $scope.colorPicker.rgb = rgb[0]+","+rgb[1]+","+rgb[2];
             $scope.colorPicker.bg = color;
             $scope.colorPicker.intensity = parseInt((rgb[3] || 1) * 255,10);
              
           }
   	  }
   })
}]);